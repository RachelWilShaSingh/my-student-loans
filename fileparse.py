import csv

# Reference: https://www.analyticsvidhya.com/blog/2021/08/python-tutorial-working-with-csv-file-for-data-science/

def OpenData():

  file = open( "student-loans-monthly.csv", "r" )

  csvReader = csv.reader( file )

  googData = []

  rows = []
  header = next( csvReader )
  header.append( "running total" )
  rows.append( header )

  runningTotal = 0

  for row in csvReader:
    if ( row[1] == "" ): continue
  
    runningTotal += float( row[1] )
    gd = [ row[0], runningTotal ]
    googData.append( gd )
    
    row.append( runningTotal )
    rows.append( row )
    
    
  file.close()
  
  return rows, googData



def SaveData( data ):
  file = open( "student-loans-b.csv", "w" )
  csvWriter = csv.writer( file )

  for row in data:
    csvWriter.writerow( row )

  file.close()


def SaveGoogleChartData( data ):
  file = open( "student-loans-goog-monthly.csv", "w" )
  
  file.write( "Date,Paid\n" )
  
  for row in data:
    file.write( "['" + row[0] + "', " + str( row[1] ) + "],\n" )
    
  file.close()
    
  
  
data, googData = OpenData()

#SaveData( data )
SaveGoogleChartData( googData )

